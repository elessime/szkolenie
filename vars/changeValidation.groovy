import pl.ttpsc.library.*;
def call() {
/**
	Template - for dedicated case or developers to define
*/
	
	if (currentBuild.rawBuild.getCauses().toString().contains('BranchIndexingCause')) {
	  print "[" + currentBuild.rawBuild.getCauses().toString() + "]"
	  print "INFO: Jenkins Indexing - Project Refresh not real execution."
	 // return 0
	}
	
	pipeline {
		agent { label 'builder' }
		
		options {
			buildDiscarder logRotator(
				artifactDaysToKeepStr: '', 
				artifactNumToKeepStr: '', 
				daysToKeepStr: '', 
				numToKeepStr: '14')
		  }
		triggers {
			bitBucketTrigger([[$class: 'BitBucketPPRRepositoryTriggerFilter', actionFilter: [$class: 'BitBucketPPRServerRepositoryPushActionFilter', allowedBranches: '', triggerAlsoIfNothingChanged: false, triggerAlsoIfTagPush: false]]])
		}
	    stages {
	    	stage('Initialization'){
	    		steps {
	                script{
						cleanWs()
						checkout scm
						bat 'echo %PATH%'
						bat 'mkdir bin'
						bat 'dir'
					}
					
	    	    }
	    	}
			
			stage('Compilation of PLM Project') {
	            steps { 
	                script{
						
						def DFExecution = new DFExecution(this);
						DFExecution.packageCompilation();
	                }
	            }
	        }
		/**
			stage('junit 5 Tests') {
	            steps { 
	                script{
						echo 'Tests'
						def dExecution = new junitExecution(this);
						dExecution.executeTests();
	                }
	            }
	        }
			
			stage('SonarQube analysis') {
				steps {
					script {
						def scannerHome = tool 'WebinarScanner';
						withSonarQubeEnv() { 
						  bat scannerHome + "/bin/sonar-scanner -Dsonar.projectKey=${BRANCH_NAME} -Dsonar.java.binaries=bin -Dsonar.java.src=src"
						}
					}
				}
			}
			
			stage("Quality Gate") {
				steps {
					script {
                      	sleep 60;
						timeout(time: 2, unit: 'MINUTES') {
							waitForQualityGate abortPipeline: true
						}
					}
				}
			}
			**/
	    }
		/**
	    post {
			always {
				archiveArtifacts artifacts: 'build/**/*.*', fingerprint: true
				junit 'build/**/*.xml'
			}	
			failure {
				// One or more steps need to be included within each condition's block.
				junit 'build/**/*.xml'
			}
	    }
		**/
	}
}
