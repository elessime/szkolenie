package pl.ttpsc.library;

class junitExecution implements Serializable {
	
	static def steps;
	
	junitExecution (steps) {
		this.steps = steps;
	}
	
	static def executeTests(){		
			def antHome = this.steps.tool name: 'WebinarAnt', type: 'ant';
      		this.steps.bat('java -version');
			this.steps.bat( '"'+ antHome + '/bin/ant"');
		}	
}
