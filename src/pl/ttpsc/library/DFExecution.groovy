package pl.ttpsc.library;

class DFExecution implements Serializable {
	
	static def steps;
	
	DFExecution (steps) {
		this.steps = steps;
	}
	
	static def packageCompilation(){		
			this.steps.withEnv(['ANT_HOME=D:\\WT_HOME\\ant', 'PATH=C:\\Program Files (x86)\\Google\\Chrome\\Application;C:\\Users\\Administrator\\Downloads\\openjdk-11+28_windows-x64_bin\\jdk-11\\bin;C:\\Windows\\system32;C:\\Windows;C:\\Windows\\System32\\Wbem;C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\;C:\\Program Files\\Git\\cmd;C:\\Program Files\\TortoiseGit\\bin;"C:\\Program Files\\Java\\jre1.8.0_241\\bin"']) {
				this.steps.bat( 'java -jar srclib/deploy.jar dist' );
			}
		}	
}
